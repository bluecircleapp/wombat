package com.kvng_willy.wombat.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kvng_willy.wombat.data.network.MyApi
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataRepository( private val api: MyApi) {
    fun getStatus(): LiveData<String> {
        val jsonResponse = MutableLiveData<String>()
        api.getRedditPost().enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                jsonResponse.value = "An error occurred"
            }
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.isSuccessful) {
                    jsonResponse.value = response.body()?.string()
                }else {
                    jsonResponse.value = "An error occurred"
                }
            }

        })
        return jsonResponse
    }
}
