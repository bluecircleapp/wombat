package com.kvng_willy.wombat.data

class Posts(
    var title:String? = null,
    var author:String? = null,
    var htmlText:String? = null,
    var url:String? = null
)