package com.kvng_willy.wombat.utility

const val children ="children"
const val data = "data"
const val title ="title"
const val author = "author"
const val url = "url"
const val htmlText = "selftext_html"
const val date = "created"

