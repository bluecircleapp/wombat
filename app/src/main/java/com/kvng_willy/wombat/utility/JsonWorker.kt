package com.kvng_willy.wombat.utility

import com.kvng_willy.wombat.data.Posts
import org.json.JSONObject

class JsonWorker {

    fun stripJson(response:String):ArrayList<Posts>{
        val redditPost = ArrayList<Posts>()
        val json = JSONObject(response)
        val jsonData = json.getJSONObject(data)
        val childrenArray = jsonData.getJSONArray(children)
        for(i in 0 until childrenArray.length()){
            val childObject = childrenArray.getJSONObject(i)
            val childData = childObject.getJSONObject(data)
            val post = Posts(childData.getString(title),childData.getString(author),childData.optString(
                htmlText),childData.getString(url))
            redditPost.add(post)
        }
        return redditPost
    }
}